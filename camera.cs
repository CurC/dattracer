﻿using OpenTK;
using System;

namespace Template
{
    class Camera
    {
        Vector3 pos, target, direction,
                up, down, left, right, p1, p2, p3;
        float aspectRatio;
        public int width { get; set; } 
        public int height { get; set; }
        public float FOV { get; set; }
        
        public Camera(Vector3 pos, float fov = 1, int width = 640, int height = 400)
        {
            this.width = width;
            this.height = height;
            this.pos = pos;
            FOV = fov;
            aspectRatio = (1f * height / width);
            target = new Vector3(0, 0, 1);
            direction = Vector3.Normalize(target - pos);
            UpdateCam();
        }

        // Update de Camera als die veranderd is
        public void UpdateCam()
        {
            direction *= FOV;
            up = new Vector3(0, 1, 0);
            left = Vector3.Normalize(Vector3.Cross(direction, up));
            down = Vector3.Normalize(Vector3.Cross(direction, left));
            right = Vector3.Normalize(Vector3.Cross(direction, down));
            p1 = pos + direction + left + up * aspectRatio;
            p2 = pos + direction + right + up * aspectRatio;
            p3 = pos + direction + left + down * aspectRatio;
            Console.WriteLine("Cam " + pos);
            Console.WriteLine("Cam Richting " + direction);
            Console.WriteLine("Left " + left);
            Console.WriteLine("Right " + right);
            Console.WriteLine("Up " + up);
            Console.WriteLine("Down " + down);
            Console.WriteLine(p1);
            Console.WriteLine(p2);
            Console.WriteLine(p3);
        }

        public void Rotate(string s)
        {
            target = pos + direction;
            switch (s)
            {
                case "L":
                    target += left;
                    break;
                case "R":
                    target += right;
                    break;
                case "D":
                    target += down;
                    break;
                case "U":
                    target += up;
                    break;
            }
            direction = Vector3.Normalize(target - pos);
            UpdateCam();
        }

        public void Translate(Vector3 T)
        {
            pos += T;
            target += T;
            direction = Vector3.Normalize(target - pos);
            UpdateCam();
        }

        public void AdjustFOV(float f)
        {
            FOV += f;
            UpdateCam();
        }

        // Camera Origin
        public Vector3 Origin
        {
            get { return pos; }
        }

        // View direction
        public Vector3 V
        {
            get { return direction; }
        }

        // Camera plane punten
        public Vector3 P1
        {
            get { return p1; }
        }

        public Vector3 P2
        {
            get { return p2; }
        }

        public Vector3 P3
        {
            get { return p3; }
        }
    }
}
