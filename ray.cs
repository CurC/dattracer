﻿using System;
using OpenTK;

namespace Template
{
    class Ray
    {
        Vector3 origin, direction;
        public float t { get; set; }

        public Ray(Vector3 O, Vector3 D)
        {
            origin = O;
            direction = Vector3.Normalize(D);
        }

        // Ray origin
        public Vector3 O
        {
            get { return origin; }
            set { origin = value; }
        }

        // Ray direction
        public Vector3 D
        {
            get { return direction; }
            set { direction = value; }
        }
    }
}
