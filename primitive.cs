﻿using System;
using OpenTK;

namespace Template
{

    abstract class Primitive
    {
        public abstract Intersection Intersect(Ray ray);
        public abstract Vector3 V { get; }
        public Vector3 Color { get; set; }
        public string ID { get; set; }
        public float Glossy { get; set; } = 0;
        public float Specular { get; set; } = 0;
        public bool IsDielectric { get; set; } = false;
        public bool IsGlossy { get; set; } = false;
        public bool IsMirror { get { return Specular != 0; } }
        public void SetSpecular(float S) { Specular = S; }
        public float IOR { get; set; }
        public float rIOR { get; set; }
        public void SetDielec(float ior) { IsDielectric = true; Color = Vector3.One; IOR = ior; rIOR = 1 / ior; }
        public void SetGlossy(float g) { Glossy = g; IsGlossy = true; }
        public float Straal { get; set; }
        public float R { get; set; }
    }

    class Plane : Primitive
    {
        Vector3 normal;
        float d;
        bool checker;
        string Type;

        public Plane(Vector3 normal, float d, Vector3 color, bool checker)
        {
            this.normal = normal;
            this.d = d;
            this.checker = checker;
            Vector3 normalT = new Vector3(Math.Abs(normal.X), Math.Abs(normal.Y), Math.Abs(normal.Z));
            Type = normalT == Vector3.UnitY ? "Y" : (normalT == Vector3.UnitY) ? "X" : "Z" ;
            normal = Vector3.Normalize(normal);
            Color = color;
            ID = "Plane";
        }

        public override Intersection Intersect(Ray ray)
        {
            float dot = Vector3.Dot(ray.D, normal);
            if (dot >= 0)
            {
                return null;
            }
            float t = -(Vector3.Dot(ray.O, normal) + d) / dot;
            Vector3 target = ray.O + (t * ray.D);
            // Checker pattern
            if (checker)
            {
                switch (Type)
                {
                    case "X":
                        Color = checker ? Vector3.One * 255 * ((int)Math.Floor(4 * target.Y) + (int)Math.Floor(4 * target.Z) & 1) : Color;
                        break;
                    case "Y":
                        Color = checker ? Vector3.One * 255 * ((int)Math.Floor(4 * target.X) + (int)Math.Floor(4 * target.Z) & 1) : Color;
                        break;
                    case "Z":
                        Color = checker ? Vector3.One * 255 * ((int)Math.Floor(4 * target.X) + (int)Math.Floor(4 * target.Y) & 1) : Color;
                        break;
                }
            }
            return new Intersection((target - ray.O).Length, this, normal, target, Color);
        }

        public override Vector3 V
        {
            get
            {
                return normal;
            }
        }
    }

    class Sphere : Primitive
    {
        Vector3 pos, normal;

        public Sphere(Vector3 pos, float straal, Vector3 color)
        {
            this.pos = pos;
            Straal = straal;
            Color = color;
            R = Straal * Straal;
            ID = "Sphere";
        }
        
        public override Intersection Intersect(Ray ray)
        {
            Vector3 p = ray.O - pos;
            float a = Vector3.Dot(ray.D, ray.D);
            float b = 2 * Vector3.Dot(ray.D, p);
            float c = Vector3.Dot(p, p) - R;
            float determinant = (b * b) - (4 * a * c);
            if (determinant < 0)
            {
                return null;
            }
            determinant = (float)Math.Sqrt(determinant);
            float t1 = (-b - determinant) / (2 * a);
            float t2 = (-b + determinant) / (2 * a);
            if (t1 < 0)
            {
                if (t2 < 0)
                {
                    ray.t = t1;
                }
                else
                {
                    ray.t = t2;
                }
            }
            else
            {
                ray.t = t1;
            }

            // ray.t = t1;
            if (ray.t < 0.001f)
            {
                return null;
            }
            Vector3 target = ray.O + (ray.t * ray.D);
            normal = Vector3.Normalize(target - pos);
            if (Vector3.Dot(normal, ray.D) > 0)
            {
                normal = -normal;
            }
            return new Intersection((target - ray.O).Length, this, normal, target, Color);
        }

        public override Vector3 V
        {
            get
            {
                return pos;
            }
        }
    }

    class Triangle : Primitive
    {
        Vector3 v1, v2, v3, N;
        float t, d;
        public Triangle(Vector3 v1, Vector3 v2, Vector3 v3, float d, Vector3 color)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
            Color = color;
            this.d = d;
            ID = "Triangle";
        }

        public override Intersection Intersect(Ray ray)
        {
            Vector3 v12 = v2 - v1;
            Vector3 v13 = v3 - v1;
            N = Vector3.Cross(v12, v13);

            float area = N.Length;

            float dot = Vector3.Dot(ray.D, N);
            if (dot >= 0)
            {
                return null;
            }

            d = Vector3.Dot(N, v1);
            t = Vector3.Dot(N, ray.O) + d / dot;
            if (t < 0) return null;
            Vector3 P = ray.O + t * ray.D;
            Vector3 C;

            Vector3 edge1 = v2 - v1;
            Vector3 vp1 = P - v1;
            C = Vector3.Cross(edge1, vp1);
            if (Vector3.Dot(N, C) < 0) return null;
            Vector3 edge2 = v3 - v2;
            Vector3 vp2 = P - v2;
            C = Vector3.Cross(edge2, vp2);
            if (Vector3.Dot(N, C) < 0) return null;
            Vector3 edge3 = v1 - v3;
            Vector3 vp3 = P - v3;
            C = Vector3.Cross(edge3, vp3);
            if (Vector3.Dot(N, C) < 0) return null;

            Vector3 target = ray.O + (t * ray.D);
            Color = Color;
            return new Intersection((target - ray.O).Length, this, N, target, Color);

        }

        public override Vector3 V
        {
            get
            {
                return N;
            }
        }
    }
}
