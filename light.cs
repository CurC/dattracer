﻿using System;
using OpenTK;

namespace Template
{
    class Light
    {
        public Vector3 pos { get; set; }
        public Vector3 color { get; set; }
        public Vector3 direction { get; set; } = Vector3.Zero;
        public float angle { get; set; } = 0;

        public Light(Vector3 pos, Vector3 color)
        {
            this.pos = pos;
            this.color = color; 
        }
    }

    class SpotLight : Light
    {
        public SpotLight(Vector3 pos, Vector3 color, Vector3 direction, float angle) : base(pos, color)
        {
            this.direction = direction;
            this.pos = pos;
            this.color = color;
            this.angle = angle;
        }
    }
}
