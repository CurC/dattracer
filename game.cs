﻿using OpenTK;
using System;
using System.IO;

namespace Template {

    class Game
    {
        Raytracer r;
        public Surface screen;

	    public void Init()
	    {
            r = new Raytracer(screen);
            r.Render(ref screen);
        }

	    // tick: renders one frame
	    public void Tick()
        {
            //screen.Clear(0);
            checkInput();
        }

        // Handelt input
        public void checkInput()
        {
            var keyboard = OpenTK.Input.Keyboard.GetState();
            if (keyboard[OpenTK.Input.Key.Keypad8])
            {
                r.scene.light[0].pos += new Vector3(0, 0, 1);
                screen.Clear(0);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Keypad2])
            {
                r.scene.light[0].pos += new Vector3(0, 0, -1);
                screen.Clear(0);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Keypad4])
            {
                r.scene.light[0].pos += new Vector3(-1, 0, 0);
                screen.Clear(0);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Keypad6])
            {
                r.scene.light[0].pos += new Vector3(1, 0, 0);
                screen.Clear(0);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Keypad3])
            {
                r.scene.light[0].pos += new Vector3(0, -1, 0);
                screen.Clear(0);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Keypad9])
            {
                r.scene.light[0].pos += new Vector3(0, 1, 0);
                screen.Clear(0);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Keypad7])
            {
                r.scene.light[0].color *= 2;
                screen.Clear(0);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Keypad1])
            {
                r.scene.light[0].color /= 2;
                screen.Clear(0);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Up])
            {
                r.cam.Rotate("U");
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Down])
            {
                r.cam.Rotate("D");
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Left])
            {
                r.cam.Rotate("L");
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Right])
            {
                r.cam.Rotate("R");
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.W])
            {
                r.cam.Translate(Vector3.UnitZ);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.S])
            {
                r.cam.Translate(-Vector3.UnitZ);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.A])
            {
                r.cam.Translate(-Vector3.UnitX);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.D])
            {
                r.cam.Translate(Vector3.UnitX);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Q])
            {
                r.cam.Translate(Vector3.UnitY);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Z])
            {
                r.cam.Translate(-Vector3.UnitY);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Plus])
            {
                r.cam.AdjustFOV(0.1f);
                r.Render(ref screen);
            }
            if (keyboard[OpenTK.Input.Key.Minus])
            {
                r.cam.AdjustFOV(-0.1f);
                r.Render(ref screen);
            }
        }
    }
    
} // namespace Template