﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Template
{
    class Scene
    {
        public Primitive[] SceneObjects { get; }
        public Light[] light { get; set; }
        Intersection hitI;

        // Primitieven en lichten
        public Scene()
        {
            // Lichten array
            light = new Light[3];
            light[0] = new Light(new Vector3(1, 1, -2), Vector3.One * 3);
            light[1] = new SpotLight(new Vector3(0, 1, -2.1f), Vector3.One * 2, new Vector3(1.1f, 1, -2.1f), 30f * (float) Math.PI/180);
            light[2] = new Light(new Vector3(0, 1, 3), Vector3.One * 2);

            // Primitieven array
            SceneObjects = new Primitive[5];
            SceneObjects[0] = new Plane(new Vector3(0, 1, 0), 0.5f, new Vector3(255, 255, 255), true);
            SceneObjects[1] = new Plane(new Vector3(0, 0, -1), 3.5f, new Vector3(255, 255, 255), true);
            SceneObjects[2] = new Sphere(new Vector3(-1.1f, 0, 0), 0.5f, new Vector3(255, 0, 0));
            SceneObjects[3] = new Sphere(new Vector3(0, 0, 0), 0.5f, new Vector3(0, 255, 0));
            SceneObjects[4] = new Sphere(new Vector3(1.1f, 0, 0), 0.5f, new Vector3(0, 0, 255));
            // SceneObjects[5] = new Triangle(new Vector3(0, 1f, 0), new Vector3(1f, 0, 0), new Vector3(0, 0, 1f), 0.5f, new Vector3(255, 0, 0));

            // Als een primitief een ander shading moet hebben dan diffuse
            SceneObjects[0].SetSpecular(0.5f);
            SceneObjects[3].SetDielec(1.49f);
            SceneObjects[4].SetSpecular(0.4f);

            hitI = new Intersection(float.PositiveInfinity, null, Vector3.Zero, Vector3.Zero, Vector3.Zero);
        }

        // Levert de kortste intersectie op
        public Intersection IntersectScene(Ray ray, float max)
        {
            hitI.Distance = max;
            for (int i = 0; i < SceneObjects.Length; i++)
            {
                if (SceneObjects[i].Intersect(ray) != null)
                {
                    if (Math.Abs(SceneObjects[i].Intersect(ray).Distance) < Math.Abs(hitI.Distance))
                    {
                        hitI = SceneObjects[i].Intersect(ray);
                    }
                }
            }
            if (hitI.Distance != max)
            {
                return hitI;
            }

            return null;
        }
    }

    class Intersection
    {
        public float Distance { get; set; }
        public Primitive Object { get; set; }
        public Vector3 Normal { get; set; }
        public Vector3 Target { get; set; }
        public Vector3 Color { get; set; }

        public Intersection(float Distance, Primitive Object, Vector3 Normal, Vector3 Target, Vector3 Color)
        {
        	this.Distance = Distance;
        	this.Object = Object;
        	this.Normal = Normal;
            this.Target = Target;
            this.Color = Color;
        }

        public override string ToString()
        {
            return "" + Distance;
        }
    }
}
